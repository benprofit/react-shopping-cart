import React from "react";
import { Home, Cart, Checkout, Confirm } from "./pages";
import { Layout } from "./components";
import "./App.css";
import { Switch, Route } from "react-router-dom";

const App = () => {
  return (
    <div className="app">
      <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/cart" component={Cart} />
          <Route path="/checkout" component={Checkout} />
          <Route path="/confirm-order" component={Confirm} />
        </Switch>
      </Layout>
    </div>
  );
};

export default App;
