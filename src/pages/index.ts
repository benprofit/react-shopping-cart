import Home from "./Home";
import Cart from "./Cart";
import Checkout from "./Checkout";
import Confirm from "./Confirm";

export { Home, Cart, Checkout, Confirm };
