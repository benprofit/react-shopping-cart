import React, { useState } from "react";
import { ProductList } from "../../components";
import Products from "../../products";
import { cartRef } from "../../firebase";

const Home = () => {
  const [products] = useState<object[]>(Products);

  const addToCart = (product: any) => {
    cartRef.push(product);
  };

  return (
    <>
      <ProductList products={products} addToCart={addToCart} />
    </>
  );
};

export default Home;
