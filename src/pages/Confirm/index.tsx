import React from "react";
import startCase from "lodash.startcase";
import emailjs from "emailjs-com";
import { cartRef } from "../../firebase";
import { useHistory } from "react-router-dom";
import "./index.css";

interface ConfirmOrderInterface {
  location: any;
}

export default (props: ConfirmOrderInterface) => {
  const { values, total } = props.location.state;
  const history = useHistory();

  const handleConfirm = (e: any) => {
    e.preventDefault();
    emailjs
      .send(
        "default_service",
        `${process.env.REACT_APP_EMAILJS_TEMPLATEID}`,
        {
          to_name: values.name,
          from_name: "Drones Store",
        },
        `${process.env.REACT_APP_EMAILJS_USERID}`
      )
      .then(
        () => {
          cartRef.root.remove();
          history.push("/");
        },
        (error) => {
          throw error;
        }
      );
  };

  return (
    <>
      {values && total ? (
        <div className="order-details">
          <h1>Confirm Order</h1>
          <div className="user-info">
            <h3>User Info:</h3>
            {Object.keys(values).map((value, x) => {
              return (
                <div key={x}>
                  <span>
                    <strong> {startCase(value)}:</strong> {values[value]}
                  </span>
                </div>
              );
            })}
            <h3>Order Summary:</h3>
            <span>
              <strong>Total: </strong>${total.total}
            </span>
          </div>
          <button className="confirm-button" onClick={(e) => handleConfirm(e)}>
            Confirm
          </button>
        </div>
      ) : (
        <div>Are your sure you provided the right info?</div>
      )}
    </>
  );
};
