import React, { useState, useEffect } from "react";
import { cartRef } from "../../firebase";
import "./index.css";
import { Link } from "react-router-dom";

export default () => {
  const [cart, setCart] = useState<any>([]);

  useEffect(() => {
    cartRef.on("value", (snapshot) => {
      let products = snapshot.val();
      let newState = [];
      for (let product in products) {
        newState.push({
          id: product,
          name: products[product].name,
          price: products[product].price,
        });
      }
      setCart(newState);
    });
  }, []);

  const getTotal = (): void => {
    return cart
      .reduce((a: any, b: any): void => a + Number(b.price), 0)
      .toFixed(2);
  };

  const removeFromCart = (product: any) => {
    cartRef.child(product.id).remove();
  };

  return (
    <div className="cart">
      {cart.length ? (
        <div className="container">
          {cart.map((product: any) => {
            return (
              <div className="cartItem" key={product.id}>
                <p>
                  <span className="cart-description">Name:</span>{" "}
                  {`${product.name}`}{" "}
                  <span className="cart-description">Price:</span>{" "}
                  {`$${product.price}`}
                </p>
                <button
                  className="remove"
                  onClick={() => removeFromCart(product)}
                >
                  Remove from cart
                </button>
              </div>
            );
          })}
          <h3>{`Cart Total: $${getTotal()}`}</h3>
          <Link
            to={{
              pathname: "/checkout",
              state: { total: getTotal() },
            }}
          >
            <button>Checkout</button>
          </Link>
        </div>
      ) : (
        <div className="empty-cart-text">
          Cart is empty! Go back and do some shopping!
        </div>
      )}
    </div>
  );
};
