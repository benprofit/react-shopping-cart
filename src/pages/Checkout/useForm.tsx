import { useState } from "react";

const useForm = (callback: any) => {
  const [values, setValues] = useState({
    name: "",
    address1: "",
    address2: "",
    zipCode: "",
    email: "",
    creditCardNumber: "",
    expDate: "",
    cvv: "",
  });
  const [errors, setErrors] = useState<Array<string>>([]);

  const handleValidation = () => {
    let fields = values;
    let allErrors = [];
    let formIsValid = true;

    //Name
    if (!fields["name"]) {
      formIsValid = false;
      allErrors.push("Name cannot be empty");
    }

    if (typeof fields["name"] !== "undefined") {
      if (!fields["name"].replace(/ /g, "").match(/^[a-zA-Z]+$/)) {
        formIsValid = false;
        allErrors.push("Name can only letters");
      }
    }

    //Email
    if (!fields["email"]) {
      formIsValid = false;
      allErrors.push("Email cannot be empty");
    }

    if (typeof fields["email"] !== "undefined") {
      let lastAtPos = fields["email"].lastIndexOf("@");
      let lastDotPos = fields["email"].lastIndexOf(".");

      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          fields["email"].indexOf("@@") === -1 &&
          lastDotPos > 2 &&
          fields["email"].length - lastDotPos > 2
        )
      ) {
        formIsValid = false;
        allErrors.push("Email is not valid");
      }
    }
    setErrors(allErrors);
    return formIsValid;
  };

  const handleSubmit = (event: any) => {
    if (event) event.preventDefault();
    if (handleValidation()) callback();
    else {
      alert(errors.join("\n"));
    }
  };

  const handleChange = (event: any) => {
    event.persist();
    setValues((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  return {
    handleChange,
    handleSubmit,
    values,
  };
};

export default useForm;
