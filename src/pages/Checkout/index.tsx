import React from "react";
import { useHistory } from "react-router-dom";
import useForm from "./useForm";
import "./index.css";

interface CheckoutInterface {
  location: any;
}

export default (props: CheckoutInterface) => {
  const history = useHistory();
  const checkout = () => {
    history.push(`/confirm-order`, { values, total: props.location.state });
  };

  const { values, handleChange, handleSubmit } = useForm(checkout);

  return (
    <form className="form" onSubmit={handleSubmit}>
      <div className="field">
        <label className="label">Full name:</label>
        <div className="control">
          <input
            className="input"
            type="name"
            name="name"
            onChange={handleChange}
            value={values.name}
            required
          />
        </div>
      </div>
      <div className="field">
        <label className="label">Address Line 1:</label>
        <div className="control">
          <input
            className="input"
            type="address1"
            name="address1"
            onChange={handleChange}
            value={values.address1}
            required
          />
        </div>
      </div>
      <div className="field">
        <label className="label">Address Line 2:</label>
        <div className="control">
          <input
            className="input"
            type="address2"
            name="address2"
            onChange={handleChange}
            value={values.address2}
          />
        </div>
      </div>
      <div className="field">
        <label className="label">Zip Code:</label>
        <div className="control">
          <input
            className="input"
            type="zipCode"
            name="zipCode"
            onChange={handleChange}
            value={values.zipCode}
            required
          />
        </div>
      </div>
      <div className="field">
        <label className="label">Email:</label>
        <div className="control">
          <input
            className="input"
            type="email"
            name="email"
            onChange={handleChange}
            value={values.email}
            required
          />
        </div>
      </div>
      <div className="field">
        <label className="label">Credit Card #:</label>
        <div className="control">
          <input
            className="input"
            type="input"
            name="creditCardNumber"
            onChange={handleChange}
            value={values.creditCardNumber}
            required
          />
        </div>
      </div>
      <div className="field">
        <label className="label">Exp. Date:</label>
        <div className="control">
          <input
            className="input"
            type="date"
            name="expDate"
            onChange={handleChange}
            value={values.expDate}
            required
          />
        </div>
      </div>
      <div className="field">
        <label className="label">CVV:</label>
        <div className="control">
          <input
            className="input"
            type="input"
            name="cvv"
            onChange={handleChange}
            value={values.cvv}
            required
          />
        </div>
      </div>
      <button type="submit" className="submit-button">
        Submit
      </button>
    </form>
  );
};
