import { v4 as uuidv4 } from "uuid";
import dji from "./images/dji-mavic.png";
import eachine from "./images/eachine-es205.png";
import iflight from "./images/iflight-dc3.png";

interface ProductInterface {
  id: string;
  name: string;
  description: string;
  picture: string;
  price: number;
}

class Product implements ProductInterface {
  public id: string;
  public name: string;
  public description: string;
  public picture: string;
  public price: number;
  public constructor(
    id: string,
    name: string,
    description: string,
    picture: string,
    price: number
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.picture = picture;
    this.price = price;
  }
}

const Products: object[] = [
  new Product(
    uuidv4(),
    "DJI Mavic Air 2",
    "A midrange drone with flagship features, the DJI Mavic Air 2 combines a foldable and portable frame with a high-end camera system.",
    dji,
    799.0
  ),
  new Product(
    uuidv4(),
    "Eachine E520S",
    "comfy white topGPS WIFI FPV With 4K/1080P HD Camera 16mins Flight Time Foldable RC Drone Quadcopter",
    eachine,
    109.99
  ),
  new Product(
    uuidv4(),
    "iFlight DC3",
    "HD SucceX-D Mini F7 TwinG 40A ESC 3 Inch FPV Racing Drone PNP BNF w/ DJI Air Unit Digital HD FPV System",
    iflight,
    359.99
  ),
];

export default Products;
