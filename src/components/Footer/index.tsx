import React from "react";
import "./index.css";

export default () => {
  return (
    <footer className="footer">
      <span>
        <p>
          Built by{" "}
          <a className="link" href="https://rubenprofit.nyc/">
            Ruben Profit
          </a>
        </p>
      </span>
    </footer>
  );
};
