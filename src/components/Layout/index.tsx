import React from "react";
import Navigation from "../Navigation";
import Footer from "../Footer";
import "./index.css";

export default ({ children }: any) => {
  return (
    <div className="layout">
      <Navigation />
      <main>{children}</main>
      <Footer />
    </div>
  );
};
