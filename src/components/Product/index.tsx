import React from "react";
import "./index.css";

interface ProductInterface {
  product: any;
}

export default (props: ProductInterface) => {
  return (
    <div className="card">
      <div className="content">
        <img
          className="hero"
          src={props.product.picture}
          alt={`${props.product.name} pic`}
        />
        <h3 className="name">{props.product.name}</h3>
        <p className="description">{props.product.description}</p>
        <p className="price">{`$${props.product.price}`}</p>
      </div>
    </div>
  );
};
