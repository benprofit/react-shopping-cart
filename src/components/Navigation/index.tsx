import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import "./index.css";
import { cartRef } from "../../firebase";

export default () => {
  const [cartCount, setCartCount] = useState<number>(0);

  useEffect(() => {
    cartRef.on("value", (snapshot) => {
      let products = snapshot.val();
      let newState = [];
      for (let product in products) {
        newState.push({
          id: product,
          name: products[product].name,
          price: products[product].price,
        });
      }
      setCartCount(newState.length);
    });
  }, []);

  const setCartCountFlag = () => (cartCount ? cartCount : null);

  return (
    <nav className="navigation">
      <div className="route">
        <NavLink className="app-name" to="/">
          Drone Store
        </NavLink>
      </div>
      <div>
        <NavLink to="/cart">
          <FontAwesomeIcon icon={faShoppingCart} />
          <span>{setCartCountFlag()}</span>
        </NavLink>
      </div>
    </nav>
  );
};
