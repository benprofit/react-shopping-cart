import React from "react";
import Product from "../Product";
import "./index.css";

interface ProductListInterface {
  products: Array<any>;
  addToCart: (product: object) => void;
}

export default (props: ProductListInterface) => {
  const { products, addToCart } = props;
  return (
    <div className="products">
      {products.map((product) => {
        return (
          <div className="card-container" key={product.id}>
            <Product product={product} />
            <button value={product.id} onClick={() => addToCart(product)}>
              Add to cart
            </button>
          </div>
        );
      })}
    </div>
  );
};
