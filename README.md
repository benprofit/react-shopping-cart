# Small shopping cart app built in react.

### Steps to run:

### clone:
```
git clone git clone https://benprofit@bitbucket.org/benprofit/react-shopping-cart.git
```

### install dependencies
```
yarn install
```

### create firebase db w/ congfig keys and IDs:

[Firebase tutorial](https://firebase.google.com/docs/database/web/start)

### create environmental variables in root dir
```
touch .env
```
then copy and paste keys in this format
```
REACT_APP_APIKEY=XXXXXXXXXXXXXXXXXX
REACT_APP_AUTHDOMAIN=XXXXXXXXXXXXXXXXX
REACT_APP_DB=XXXXXXXXXXXXXXXXXXX
REACT_APP_PID=XXXXXXXXXXXXXX
REACT_APP_SB=XXXXXXXXXXXXXXXX
REACT_APP_SID=XXXXXXXXXXXXXXX
REACT_APP_APPID=1:XXXXXXX:web:XXXXXXXXXXXX
REACT_APP_MID=G-XXXXXXXXXXXXXX
```
### create emailjs account and template (preferably GMAIl or other personal):

[EmailJS](https://www.emailjs.com/docs/)

add template ID and use ID to .env file
```
REACT_APP_EMAILJS_USERID=XXXXXXXXXXXXXXX
REACT_APP_EMAILJS_TEMPLATEID=XXXXXXXXXXXXXX
```

### run locally:
```
yarn start
```
